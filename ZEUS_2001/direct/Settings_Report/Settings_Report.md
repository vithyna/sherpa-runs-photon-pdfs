---
title: Sherpa run-time settings
date: Wed Mar 15 01:25:57 2023
...

Customised settings
-------------------
The parameters listed here have been customised in some way and they have been read by Sherpa during its run. The last column lists the actual value used after taking into account all setting sources (default values, overrides, input files and the command line).

In some cases, an alternative default value is being used. These alternatives will be separated by "`-- AND --`" from the standard default, which will always be listed on top.

Note that parameters that can take on different values because they are set within a list, for example `param: [{x: 1}, {x: 2}, ...]`, will not appear in the config-file or command-line columns. They will be listed in the final-value column, with each different value separated by an "`-- AND --`" line.

| parameter | default value | override by SHERPA | Sherpa.yaml | command line | final value |
|-|-|-|-|-|-|
ALPHAS:USE\_PDF| 1 |  | 0 |  | 0 |  |
ALPHAS\(MZ\)| 0\.118 |  | 0\.1188 |  | 0\.1188 |  |
ANALYSIS|  |  | Rivet |  | Rivet |  |
BEAMS| 0, 0 |  | \-11, 2212 |  | \-11, 2212 |  |
BEAM\_ENERGIES| 0, 0 |  | 27\.5, 820 |  | 27\.5, 820 |  |
BEAM\_SPECTRA| Monochromatic, Monochromatic |  | EPA, Monochromatic |  | EPA, Monochromatic |  |
EVENTS| 100 |  | 3M |  | 3000000 |  |
EVENT\_GENERATION\_MODE| PartiallyUnweighted |  | W |  | W |  |
MEPS:ALLOW\_SCALE\_UNORDERING| \-\- AND \-\-<br />0 |  |  |  | 0 |  |
MEPS:CLUSTER\_MODE| \-\- AND \-\-<br />360 |  |  |  | 360 |  |
MEPS:CORE\_SCALE| \-\- AND \-\-<br />Default |  |  |  | Default |  |
MEPS:MEPS\_COLORSET\_MODE| \-\- AND \-\-<br />0 |  |  |  | 0 |  |
MEPS:NLO\_COUPLING\_MODE| \-\- AND \-\-<br />2 |  |  |  | 2 |  |
MEPS:NLO\_NMAX\_ALLCONFIGS| \-\- AND \-\-<br />\-1 |  |  |  | \-1 |  |
MEPS:NMAX\_ALLCONFIGS| \-\- AND \-\-<br />\-1 |  |  |  | \-1 |  |
MEPS:UNORDERED\_SCALE| \-\- AND \-\-<br />None |  |  |  | None |  |
ME\_GENERATORS| Comix, Amegic, Internal |  | Comix, Amegic |  | Comix, Amegic |  |
MI\_HANDLER| Amisic |  | 0 |  | None |  |
ORDER\_ALPHAS| 2 |  | 1 |  | 1 |  |
PARTICLE\_DATA:4:Massive| 0 |  | true |  | 1 |  |
PARTICLE\_DATA:5:Massive| 0 |  | true |  | 1 |  |
PDF\_LIBRARY|  |  | None, LHAPDFSherpa |  | None, LHAPDFSherpa |  |
PDF\_SET|  |  | None, NNPDF31\_nnlo\_as\_0118\_proton |  | None, NNPDF31\_nnlo\_as\_0118\_proton |  |
PROCESSES:22 93 \-\> \(93,4,5\) \(93,\-4,\-5\) 93\{1\}:CKKW|  |  |  |  | 9 |  |
PROCESSES:22 93 \-\> \(93,4,5\) \(93,\-4,\-5\) 93\{1\}:Order:EW| \-1 |  |  |  | 1 |  |
PROCESSES:22 93 \-\> \(93,4,5\) \(93,\-4,\-5\) 93\{1\}:Order:QCD| \-1 |  |  |  | 1 |  |
RIVET:\-\-analyses|  |  | ZEUS\_2001\_S4815815 |  | ZEUS\_2001\_S4815815 |  |
RIVET:\-\-ignore\-beams|  |  | 1 |  | 1 |  |
RIVET:USE\_HEPMC\_SHORT| 0 |  | 1 |  | 1 |  |
SCALES| METS\{MU\_F2\}\{MU\_R2\}\{MU\_Q2\} |  | METS\{H\_T2\}\{H\_T2\} |  | METS\{H\_T2\}\{H\_T2\} |  |
SELECTORS:NJetFinder:ETMin| None |  |  |  | 9 |  |
SELECTORS:NJetFinder:EtaMax| None |  |  |  | 1\.79769313486e\+308 |  |
SELECTORS:NJetFinder:MassMax| 0 |  |  |  | 5 |  |
SELECTORS:NJetFinder:N| None |  |  |  | 2 |  |
SELECTORS:NJetFinder:PTMin| None |  |  |  | 0 |  |
SELECTORS:NJetFinder:R| 0\.4 |  |  |  | 1 |  |
SELECTORS:NJetFinder:YMax| None |  |  |  | 1\.79769313486e\+308 |  |
Settings kept at their default value
-------------------
The parameter listed here have not been customised, but they have been read by Sherpa during its run.

| parameter | default value |
|-|-|
1/ALPHAQED\(0\)| 137\.03599976 |  |
ABS\_ERROR| 0 |  |
AHADIC:ALPHA\_B| 2\.5 |  |
AHADIC:ALPHA\_D| 3\.26 |  |
AHADIC:ALPHA\_G| 0\.67 |  |
AHADIC:ALPHA\_H| 1\.26 |  |
AHADIC:ALPHA\_L| 2\.5 |  |
AHADIC:BARYON\_FRACTION| 0\.15 |  |
AHADIC:BEAUTYBARYON\_ENHANCEMENT| 0\.8 |  |
AHADIC:BEAUTYCHARM\_ENHANCEMENT| 1 |  |
AHADIC:BEAUTYSTRANGE\_ENHANCEMENT| 1\.4 |  |
AHADIC:BETA\_B| 0\.25 |  |
AHADIC:BETA\_D| 0\.11 |  |
AHADIC:BETA\_H| 0\.98 |  |
AHADIC:BETA\_L| 0\.13 |  |
AHADIC:CHARMBARYON\_ENHANCEMENT| 8 |  |
AHADIC:CHARMSTRANGE\_ENHANCEMENT| 2 |  |
AHADIC:CLUSTER\_SPLITTING\_MODE| 2 |  |
AHADIC:DECAY\_THRESHOLD| 0\.02 |  |
AHADIC:DIRECT\_TRANSITIONS| 1 |  |
AHADIC:DI\_PION\_THRESHOLD| 0\.3 |  |
AHADIC:ETA\_MODIFIER| 2\.2 |  |
AHADIC:ETA\_PRIME\_MODIFIER| 4\.5 |  |
AHADIC:GAMMA\_B| 0\.5 |  |
AHADIC:GAMMA\_D| 0\.39 |  |
AHADIC:GAMMA\_H| 0\.05 |  |
AHADIC:GAMMA\_L| 0\.27 |  |
AHADIC:GLUON\_DECAY\_MODE| 0 |  |
AHADIC:KT\_0| 1\.34 |  |
AHADIC:KT\_ORDER| 0 |  |
AHADIC:MASS\_EXPONENT| 0 |  |
AHADIC:MIN\_MASS2| 0\.1 |  |
AHADIC:MULTI\_WEIGHT\_R0L0\_DELTA\_3/2| 0\.15 |  |
AHADIC:MULTI\_WEIGHT\_R0L0\_N\_1/2| 1 |  |
AHADIC:MULTI\_WEIGHT\_R0L0\_PSEUDOSCALARS| 1 |  |
AHADIC:MULTI\_WEIGHT\_R0L0\_TENSORS2| 1\.5 |  |
AHADIC:MULTI\_WEIGHT\_R0L0\_VECTORS| 2\.5 |  |
AHADIC:MULTI\_WEIGHT\_R0L1\_AXIALVECTORS| 0 |  |
AHADIC:MULTI\_WEIGHT\_R0L1\_SCALARS| 0 |  |
AHADIC:MULTI\_WEIGHT\_R0L2\_VECTORS| 0\.5 |  |
AHADIC:MULTI\_WEIGHT\_R1L0\_N\_1/2| 0\.1 |  |
AHADIC:MULTI\_WEIGHT\_R1\_1L0\_N\_1/2| 0 |  |
AHADIC:MULTI\_WEIGHT\_R2L0\_N\_1/2| 0 |  |
AHADIC:M\_BIND\_0| 0\.12 |  |
AHADIC:M\_BIND\_1| 0\.5 |  |
AHADIC:M\_BOTTOM| 5\.1 |  |
AHADIC:M\_CHARM| 1\.8 |  |
AHADIC:M\_DIQUARK\_OFFSET| 0\.3 |  |
AHADIC:M\_GLUE| 0 |  |
AHADIC:M\_STRANGE| 0\.4 |  |
AHADIC:M\_UP\_DOWN| 0\.3 |  |
AHADIC:Mixing\_0\+| \-0\.246091424531 |  |
AHADIC:Mixing\_1\-| 0\.635299847726 |  |
AHADIC:Mixing\_2\+| 0\.471238898038 |  |
AHADIC:Mixing\_3\-| 0\.5411 |  |
AHADIC:Mixing\_4\+| 0\.6283 |  |
AHADIC:OPEN\_THRESHOLD| 0\.1 |  |
AHADIC:PI\_PHOTON\_THRESHOLD| 0\.15 |  |
AHADIC:PROMPT\_DECAY\_EXPONENT| \-1 |  |
AHADIC:PT\_MAX| 0\.68 |  |
AHADIC:P\_QQ1\_by\_P\_QQ0| 0\.94 |  |
AHADIC:P\_QS\_by\_P\_QQ\_norm| 0\.71 |  |
AHADIC:P\_SS\_by\_P\_QQ\_norm| 0\.01 |  |
AHADIC:REMNANT\_CLUSTER\_MODE| 2 |  |
AHADIC:SINGLETBARYON\_MODIFIER| 1\.8 |  |
AHADIC:SINGLET\_MODIFIER| 2 |  |
AHADIC:STRANGE\_FRACTION| 0\.46 |  |
AHADIC:TRANSITION\_THRESHOLD| 0\.51 |  |
ALIASDECAYFILE| AliasDecays\.dat |  |
ALLOW\_MOMENTUM\_NONCONSERVATION| 1 |  |
ALPHAS:FREEZE\_VALUE| 1 |  |
ALPHAS:PDF\_SET\_VERSION| 0 |  |
AMEGIC:DEFAULT\_GAUGE| 1 |  |
AMEGIC:PARTIAL\_COMMIT| 0 |  |
ANALYSIS\_OUTPUT| Analysis/ |  |
ANALYSIS\_WRITEOUT\_INTERVAL| 1\.84467440737e\+19 |  |
ASSOCIATED\_CONTRIBUTIONS\_VARIATIONS|  |  |
AS\_FORM| Smooth |  |
BATCH\_MODE| 1 |  |
BEAM\_1| 0 |  |
BEAM\_2| 0 |  |
BEAM\_ENERGY\_1| 0 |  |
BEAM\_ENERGY\_2| 0 |  |
BEAM\_MODE| Collider |  |
BEAM\_POLARIZATIONS| 0, 0 |  |
BEAM\_REMNANTS| 1 |  |
BEAM\_SMAX| 1 |  |
BEAM\_SMIN| 1e\-10 |  |
BEAM\_SPECTRUM\_1| Monochromatic |  |
BEAM\_SPECTRUM\_2| Monochromatic |  |
BRH\_VMODE| 0 |  |
BUNCHES|  |  |
B\_DECAYFILE| Partonic\_b/Decays\.dat |  |
CHECK\_LIBLOCK| 0 |  |
CHECK\_SETTINGS| 1 |  |
CHECK\_WEIGHT| 0 |  |
CITATION\_DEPTH| 1 |  |
CI\_OMODE| 1 |  |
CKM:Order| 0 |  |
CKM:Output| 0 |  |
COLOUR\_RECONNECTIONS:Mode| 0 |  |
COLOUR\_SCHEME| 0 |  |
COMIX:AEXP| 0\.9 |  |
COMIX:BMODE| 1 |  |
COMIX:CHECK\_POLES| 0 |  |
COMIX:ECMODE| 2 |  |
COMIX:ITMAX| 1000000 |  |
COMIX:ITMIN| 1000 |  |
COMIX:MFAC| 1 |  |
COMIX:N\_GPL| 3 |  |
COMIX:OMODE| 3 |  |
COMIX:PARTIAL\_COMMIT| 0 |  |
COMIX:PG\_MODE| 0 |  |
COMIX:PMODE| D |  |
COMIX:PS\_ADD\_ANISO| 0 |  |
COMIX:PS\_CHTH| 0\.01 |  |
COMIX:SEXP| 1 |  |
COMIX:SPEAK| 1 |  |
COMIX:SRBASE| 1\.05 |  |
COMIX:STEXP| 0\.001 |  |
COMIX:TEXP| 0\.9 |  |
COMIX:THEXP| 1\.5 |  |
COMIX:TMODE| 1 |  |
COMIX:VINTS| 8 |  |
COMIX:VL\_MODE| 0 |  |
COMIX:VMODE| 1 |  |
COMIX:VSOPT| 1 |  |
COMIX:WF\_MODE| 0 |  |
COMIX:ZMODE| 0 |  |
COMIX\_DEFAULT\_GAUGE| 1 |  |
COMPRESS\_PARTONIC\_DECAYS| 1 |  |
COUPLINGS| Alpha\_QCD 1 |  |
CSS\_CKFMODE| 1 |  |
CSS\_ENHANCE|  |  |
CSS\_EVOLUTION\_SCHEME| 30 |  |
CSS\_EW\_MODE| 0 |  |
CSS\_FS\_AS\_FAC| 1 |  |
CSS\_FS\_PT2MIN| 1 |  |
CSS\_IS\_AS\_FAC| 1 |  |
CSS\_IS\_PT2MIN| 2 |  |
CSS\_KFACTOR\_SCHEME| 9 |  |
CSS\_KIN\_SCHEME| 1 |  |
CSS\_KMODE| 2 |  |
CSS\_MASS\_THRESHOLD| 0 |  |
CSS\_MAXEM| 18446744073709551615 |  |
CSS\_MAXPART| 2147483647 |  |
CSS\_MAX\_REWEIGHT\_FACTOR| 1000 |  |
CSS\_PDFCHECK| 1 |  |
CSS\_PDF\_FAC| 1 |  |
CSS\_PDF\_MIN| 0\.0001 |  |
CSS\_PDF\_MIN\_X| 0\.01 |  |
CSS\_QCD\_MODE| 1 |  |
CSS\_RECO\_CHECK| 0 |  |
CSS\_RECO\_DECAYS| 0 |  |
CSS\_RESPECT\_Q2| 0 |  |
CSS\_REWEIGHT| 1 |  |
CSS\_REWEIGHT\_SCALE\_CUTOFF| 5 |  |
CSS\_SCALE\_FACTOR| 1 |  |
CSS\_SCALE\_SCHEME| 20 |  |
CSS\_SCALE\_VARIATION\_SCHEME| 1 |  |
C\_DECAYFILE| Partonic\_c/Decays\.dat |  |
DEBUG\_INTERVAL| 0 |  |
DEBUG\_STEP| \-1 |  |
DECAYCONSTFILE| HadronConstants\.dat |  |
DECAYDATA| /home/anarendran/Documents/sherpa/sherpa/local/share/SHERPA\-MC//Decaydata\.zip |  |
DECAYER| 0 |  |
DECAYFILE| HadronDecays\.dat |  |
DECAYMODEL| Hadrons |  |
DECAYPATH| /home/anarendran/Documents/sherpa/sherpa/local/share/SHERPA\-MC// |  |
DECOMPOSE\_4G\_VERTEX| 1 |  |
DIPOLES:ALPHA| 1 |  |
DIPOLES:ALPHA\_FF| 1 |  |
DIPOLES:ALPHA\_FI| 1 |  |
DIPOLES:ALPHA\_IF| 1 |  |
DIPOLES:ALPHA\_II| 1 |  |
DIPOLES:AMIN| 1e\-08 |  |
DIPOLES:KAPPA| 0\.666666666667 |  |
DIPOLES:KT2MAX| 90200\.8508303 |  |
DIPOLES:NF\_GSPLIT| 3 |  |
DIPOLES:SCHEME| CSS |  |
DM\_ENERGY\_DISTRIBUTION| 1 |  |
DM\_RELATIVISTIC| 1 |  |
DM\_TEMPERATURE| 1 |  |
DM\_beam\_weighted| 1 |  |
ENHANCE\_XS| 0 |  |
EPA:AlphaQED| 0\.0072992701 |  |
EPA:Debug| 0 |  |
EPA:Form\_Factor| 0 |  |
EPA:PTMin| 0 |  |
EPA:Q2Max| 3 |  |
EPA:ThetaMax| 0\.3 |  |
EPA:Use\_old\_WW| 0 |  |
EPA:XMin| 0 |  |
ERROR| 0\.01 |  |
EVENT\_DISPLAY\_INTERVAL| 100 |  |
EVENT\_INPUT|  |  |
EVENT\_OUTPUT|  |  |
EVENT\_SEED\_FILE| ran\.stat\.1234 |  |
EVENT\_SEED\_MODE| 0 |  |
EVENT\_TYPE| StandardPerturbative |  |
EVT\_FILE\_PATH| \. |  |
EVT\_OUTPUT| 2 |  |
EVT\_OUTPUT\_START| 0 |  |
EW\_REN\_SCHEME| Gmu |  |
EW\_SCHEME| Gmu |  |
EXTERNAL\_RNG| None |  |
EXTRAXS\_CSS\_APPROX\_ME| 0 |  |
FACTORIZATION\_SCALE\_FACTOR| 1 |  |
FINISH\_OPTIMIZATION| 1 |  |
FLAG\_PARTONIC\_DECAYS| 1 |  |
FRAGMENTATION| Ahadic |  |
FREEZE\_PDF\_FOR\_LOW\_Q| 0 |  |
GENERATE\_RESULT\_DIRECTORY| 1 |  |
GF| 1\.16639e\-05 |  |
GLOBAL\_KFAC| 0 |  |
GMU\_CMS\_AQED\_CONVENTION| 0 |  |
HADRONALIASESFILE| HadronAliases\.dat |  |
HADRON\_DECAYS\_QED\_CORRECTIONS| 1 |  |
HARD\_DECAYS:Enabled| 0 |  |
HARD\_SPIN\_CORRELATIONS| 0 |  |
HELICITY\_SCHEME| 1 |  |
HEPMC\_EXTENDED\_WEIGHTS| 0 |  |
HEPMC\_TREE\_LIKE| 0 |  |
HEPMC\_USE\_NAMED\_WEIGHTS| 1 |  |
HISTOGRAM\_OUTPUT\_PRECISION| 6 |  |
IB\_THRESHOLD\_KILL| \-1e\+12 |  |
IB\_WHBINS| 100 |  |
INIT\_ONLY| 0 |  |
INTEGRATION\_ERROR| 0\.01 |  |
INTEGRATOR| Default |  |
INTRINSIC\_KPERP:CUT\_EXPO| 5 |  |
INTRINSIC\_KPERP:FORM| gauss\_limited |  |
INTRINSIC\_KPERP:MAX| 3 |  |
INTRINSIC\_KPERP:MEAN| 0 |  |
INTRINSIC\_KPERP:Q2| 0\.77 |  |
INTRINSIC\_KPERP:REFE| 7000 |  |
INTRINSIC\_KPERP:SCALE\_EXPO| 0\.08 |  |
INTRINSIC\_KPERP:SIGMA| 1\.5 |  |
INT\_MINSIJ\_FACTOR| 0 |  |
JET\_CRITERION| CSS |  |
JET\_MASS\_THRESHOLD| 10 |  |
KFACTOR| None |  |
KFACTOR\_ALLOW\_MAPPING| 1 |  |
LHAPDF:DISALLOW\_FLAVOUR|  |  |
LHAPDF:USE\_Q2LIMIT| 1 |  |
LHEF\_PDF\_NUMBER| \-1 |  |
LOG\_FILE|  |  |
MASSIVE\_PS|  |  |
MASSLESS\_PS|  |  |
MAX\_PROPER\_LIFETIME| 10 |  |
MCNLO\_DADS| 1 |  |
MEH\_EWADDMODE| 0 |  |
MEH\_NLOADD| 1 |  |
MEH\_QCDADDMODE| 0 |  |
MEMLEAK\_WARNING\_THRESHOLD| 16777216 |  |
MENLOPS\_MAX\_KFAC| 10 |  |
MEPSNLO\_PDFCT| 1 |  |
METS:CLUSTER\_MODE| 0 |  |
METS\_BBAR\_MODE| EnabledExclCluster |  |
ME\_QED:CLUSTERING\_ENABLED| 1 |  |
ME\_QED:CLUSTERING\_THRESHOLD| 10 |  |
ME\_QED:ENABLED| 1 |  |
ME\_QED:INCLUDE\_RESONANCES| 0 |  |
MI\_CSS\_FS\_AS\_FAC| 0\.66 |  |
MI\_CSS\_FS\_PT2MIN| 1 |  |
MI\_CSS\_IS\_AS\_FAC| 0\.66 |  |
MI\_CSS\_IS\_PT2MIN| 4 |  |
MI\_CSS\_KFACTOR\_SCHEME| 0 |  |
MI\_CSS\_KIN\_SCHEME| 1 |  |
MI\_PDF\_LIBRARY|  |  |
MODEL| SM |  |
MPI\_EVENT\_MODE| 0 |  |
MPI\_OUTPUT| 0 |  |
MPI\_PDF\_SET|  |  |
MPI\_PDF\_SET\_VERSIONS|  |  |
MPI\_PT\_MAX| 1e\+12 |  |
MPI\_PT\_Max\_Fac| 1 |  |
MPI\_SEED\_MODE| 0 |  |
NLO\_IMODE| IKP |  |
NLO\_MUR\_COEFFICIENT\_FROM\_VIRTUAL| 1 |  |
NLO\_NF\_CONVERSION\_TERMS| None |  |
NLO\_SMEAR\_POWER| 0\.5 |  |
NLO\_SMEAR\_THRESHOLD| 0 |  |
NLO\_SUBTRACTION\_MODE| QCD |  |
NO\_ZERO\_PDF| 0 |  |
NUM\_ACCURACY| 1e\-10 |  |
OUTPUT| 2 |  |
OUTPUT\_ME\_ONLY\_VARIATIONS| 1 |  |
OVERRIDE\_PDF\_INFO| 0 |  |
OVERWEIGHT\_THRESHOLD| 1e\+12 |  |
PB\_USE\_FMM| 0 |  |
PDF\_SET\_VERSIONS|  |  |
PRETTY\_PRINT| On |  |
PRINT\_PS\_POINTS| 0 |  |
PRINT\_VERSION\_INFO| 0 |  |
PROCESSES:22 93 \-\> \(93,4,5\) \(93,\-4,\-5\) 93\{1\}:Cut\_Core| 0 |  |
PROCESSES:22 93 \-\> \(93,4,5\) \(93,\-4,\-5\) 93\{1\}:Decay|  |  |
PROCESSES:22 93 \-\> \(93,4,5\) \(93,\-4,\-5\) 93\{1\}:DecayOS|  |  |
PROCESSES:22 93 \-\> \(93,4,5\) \(93,\-4,\-5\) 93\{1\}:No\_Decay|  |  |
PS\_PT\_FILE|  |  |
Q2\_AS| 1 |  |
RANDOM\_SEED| \-1, \-1, \-1, \-1 |  |
RANDOM\_SEED1| \-1 |  |
RANDOM\_SEED2| \-1 |  |
RANDOM\_SEED3| \-1 |  |
RANDOM\_SEED4| \-1 |  |
RELIC\_DENSITY\_EMAX| 1000000 |  |
REMNANTS:DELTA\_MASS| 1\.5 |  |
REMNANTS:SOFT\_ETA\_RANGE| 7\.5 |  |
REMNANTS:SOFT\_MASS| 5 |  |
REMNANTS:SOFT\_X\_EXPONENT| \-2 |  |
RENORMALIZATION\_SCALE\_FACTOR| 1 |  |
RESPECT\_MASSIVE\_FLAG| 0 |  |
RESULT\_DIRECTORY| Results |  |
RESUMMATION\_SCALE\_FACTOR| 1 |  |
REWEIGHT\_SPLITTING\_ALPHAS\_SCALES| 1 |  |
REWEIGHT\_SPLITTING\_PDF\_SCALES| 1 |  |
RIVET:\-l| 20 |  |
RIVET:HISTO\_INTERVAL| 0 |  |
RIVET:JETCONTS| 0 |  |
RIVET:MATCH\_WEIGHTS|  |  |
RIVET:NLO\_SMEARING| 0 |  |
RIVET:NOMINAL\_WEIGHT|  |  |
RIVET:SKIP\_WEIGHTS| 0 |  |
RIVET:SPLITCOREPROCS| 0 |  |
RIVET:SPLITPM| 0 |  |
RIVET:SPLITSH| 0 |  |
RIVET:UNMATCH\_WEIGHTS| ^EXTRA\_\_\.\*,^IRREG\_\_\.\* |  |
RIVET:USE\_HEPMC\_EXTENDED\_WEIGHTS| 0 |  |
RIVET:USE\_HEPMC\_NAMED\_WEIGHTS| 1 |  |
RIVET:USE\_HEPMC\_TREE\_LIKE| 0 |  |
RIVET:WEIGHT\_CAP| 0 |  |
RLIMIT\_AS| 33472172032 |  |
RLIMIT\_BY\_CPU| 0 |  |
RUNDATA| Sherpa\.yaml |  |
RUN\_MASS\_BELOW\_POLE| 0 |  |
SAVE\_STATUS|  |  |
SCALE\_FACTOR| 1 |  |
SELECTION\_WEIGHT\_MODE| 0 |  |
SELECTORS:NJetFinder:Exp| 1 |  |
SELECTORS:NJetFinder:Mode| 2 |  |
SHERPA\_CPP\_PATH|  |  |
SHERPA\_LDADD|  |  |
SHERPA\_LIB\_PATH|  |  |
SHERPA\_VERSION|  |  |
SHOWER\_GENERATOR| CSS |  |
SHOW\_ANALYSIS\_SYNTAX| 0 |  |
SHOW\_FILTER\_SYNTAX| 0 |  |
SHOW\_KFACTOR\_SYNTAX| 0 |  |
SHOW\_ME\_GENERATORS| 0 |  |
SHOW\_MODEL\_SYNTAX| 0 |  |
SHOW\_NLOMC\_GENERATORS| 0 |  |
SHOW\_NTRIALS| 0 |  |
SHOW\_PDF\_SETS| 0 |  |
SHOW\_PS\_GENERATORS| 0 |  |
SHOW\_SCALE\_SYNTAX| 0 |  |
SHOW\_SELECTOR\_SYNTAX| 0 |  |
SHOW\_SHOWER\_GENERATORS| 0 |  |
SHOW\_VARIABLE\_SYNTAX| 0 |  |
SOFT\_COLLISIONS| None |  |
SOFT\_MASS\_SMEARING| 1 |  |
SOFT\_SPIN\_CORRELATIONS| 0 |  |
SP:ADD\_DOC| 0 |  |
SP:SET\_COLORS| 0 |  |
SPECIAL\_TAU\_SPIN\_CORRELATIONS| 0 |  |
STATUS\_PATH|  |  |
THRESHOLD\_ALPHAS| 1 |  |
TIMEOUT| \-1 |  |
USERHOOKS| None |  |
USR\_WGT\_MODE| 1 |  |
VARIATIONS\_INCLUDE\_CV| 0 |  |
VEGAS\_MODE| 2 |  |
VIRTUAL\_EVALUATION\_FRACTION| 1 |  |
WIDTH\_SCHEME| CMS |  |
WRITE\_REFERENCES\_FILE| 1 |  |
YFS:1/ALPHAQED| 0 |  |
YFS:CHECK\_FIRST| 0 |  |
YFS:DRCUT| 1\.79769313486e\+308 |  |
YFS:FF\_RECOIL\_SCHEME| 2 |  |
YFS:FI\_RECOIL\_SCHEME| 2 |  |
YFS:INCREASE\_MAXIMUM\_WEIGHT| 1 |  |
YFS:IR\_CUTOFF| 0\.001 |  |
YFS:IR\_CUTOFF\_FRAME| Multipole\_CMS |  |
YFS:MAXEM| 2147483647 |  |
YFS:MINEM| 0 |  |
YFS:MODE| Full |  |
YFS:PHOTON\_SPLITTER\_ENHANCE\_FACTOR| 1 |  |
YFS:PHOTON\_SPLITTER\_MAX\_HADMASS| 0\.5 |  |
YFS:PHOTON\_SPLITTER\_MODE| 15 |  |
YFS:PHOTON\_SPLITTER\_ORDERING\_SCHEME| 2 |  |
YFS:PHOTON\_SPLITTER\_SPECTATOR\_SCHEME| 0 |  |
YFS:PHOTON\_SPLITTER\_STARTING\_SCALE\_SCHEME| 1 |  |
YFS:REDUCE\_MAXIMUM\_ENERGY| 1 |  |
YFS:STRICTNESS| 0 |  |
YFS:USE\_ME| 1 |  |
YFS:USE\_RUNNING\_PARAMETERS| 0 |  |
YFS:UV\_CUTOFF| 1\.79769313486e\+308 |  |
YUKAWA\_MASSES| Running |  |
