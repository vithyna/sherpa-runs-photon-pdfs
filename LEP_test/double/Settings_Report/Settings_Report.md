---
title: Sherpa run-time settings
date: Fri Jun  7 15:31:09 2024
...

Unused settings
-------------------
Parameters that have never been read by Sherpa during its run are listed here. If you did expect the setting to be used, check its spelling, and note that Sherpa setting names are case-sensitive.

### Sherpa.yaml
- PARTICLE_CONTAINERS:996:Flavours
- PARTICLE_CONTAINERS:996:Flavours
- PARTICLE_CONTAINERS:997:Flavours
- PARTICLE_CONTAINERS:997:Flavours
- RIVET:--analyses


Customised settings
-------------------
The parameters listed here have been customised in some way and they have been read by Sherpa during its run. The last column lists the actual value used after taking into account all setting sources (default values, overrides, input files and the command line).

In some cases, an alternative default value is being used. These alternatives will be separated by "`-- AND --`" from the standard default, which will always be listed on top.

Note that parameters that can take on different values because they are set within a list, for example `param: [{x: 1}, {x: 2}, ...]`, will not appear in the config-file or command-line columns. They will be listed in the final-value column, with each different value separated by an "`-- AND --`" line.

| parameter | default value | override by SHERPA | /home/narendran/Documents/CEDAR_stuff2/sherpa/local/share/SHERPA-MC//Decaydata.yaml | Sherpa.yaml | command line | final value |
|-|-|-|-|-|-|-|
AMISIC:PT\_0| 1\.15878964346 |  |  | 2\. |  | 2 |  |
AMISIC:PT\_Min| 2\.25 |  |  | 1\.5 |  | 1\.5 |  |
AMISIC:SIGMA\_ND\_NORM| 1\.02 |  |  | 1\.05 |  | 1\.05 |  |
ANALYSIS|  |  |  | Rivet |  | Rivet |  |
BEAMS| 0, 0 |  |  | \-11, 11 |  | \-11, 11 |  |
BEAM\_ENERGIES| 0, 0 |  |  | 99 |  | 99 |  |
BEAM\_SPECTRA| Monochromatic, Monochromatic |  |  | EPA |  | EPA |  |
DIPOLES:NF\_GSPLIT| 3<br />\-\- AND \-\-<br />3 |  |  |  |  | 3 |  |
EPA:Q2Max| 3 |  |  | 4\.5 |  | 4\.5 |  |
EVENTS| 100 |  |  | 500k | 1k | 1000 |  |
EVENT\_GENERATION\_MODE| PartiallyUnweighted |  |  | W |  | W |  |
ME\_GENERATORS| Comix, Amegic, Internal |  |  | Comix, Amegic, OpenLoops |  | Comix, Amegic, OpenLoops |  |
MPI\_PDF\_LIBRARY|  |  |  | SASGSherpa |  | SASGSherpa |  |
MPI\_PDF\_SET|  |  |  | SAS2D |  | SAS2D |  |
NLO\_SUBTRACTION\_MODE| QCD |  |  | QCD\+QED |  | QCD\|QED |  |
PARTICLE\_CONTAINERS:996:Flavs|  |  |  |  |  | 4, \-4 |  |
PARTICLE\_CONTAINERS:996:Name| 996 |  |  | C |  | C |  |
PARTICLE\_CONTAINERS:997:Flavs|  |  |  |  |  | 5, \-5 |  |
PARTICLE\_CONTAINERS:997:Name| 997 |  |  | B |  | B |  |
PARTICLE\_DATA:4:Massive| 0 |  |  | true |  | 1 |  |
PARTICLE\_DATA:5:Massive| 0 |  |  | true |  | 1 |  |
PDF\_LIBRARY|  |  |  | SASGSherpa |  | SASGSherpa |  |
PDF\_SET|  |  |  | SAS2D |  | SAS2D |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):Loop\_Generator|  |  |  | OpenLoops |  | OpenLoops |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):ME\_Generator|  |  |  | Amegic |  | Amegic |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):NLO\_Mode|  |  |  | MC@NLO |  | MC@NLO |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):NLO\_Order:EW| \-1 |  |  | 0 |  | 0 |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):NLO\_Order:QCD| \-1 |  |  | 1 |  | 1 |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):Order:EW| \-1 |  |  | 0 |  | 0 |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):Order:QCD| \-1 |  |  | 2 |  | 2 |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):RS\_ME\_Generator|  |  |  | Comix |  | Comix |  |
RIVET:ANALYSES|  |  |  |  |  | OPAL\_2003\_I611415 |  |
RIVET:USE\_HEPMC\_SHORT| 0 |  |  | 1 |  | 1 |  |
SCALES| METS\{MU\_F2\}\{MU\_R2\}\{MU\_Q2\} |  |  | METS\{max\(4\.,H\_T2/4\)\}\{max\(4\.,H\_T2/4\)\}\{max\(4\.,H\_T2/16\)\} |  | METS\{max\(4\.,H\_T2/4\)\}\{max\(4\.,H\_T2/4\)\}\{max\(4\.,H\_T2/16\)\} |  |
SELECTORS:0:NJetFinder:ETMin| None |  |  |  |  | 0 |  |
SELECTORS:0:NJetFinder:EtaMax| None |  |  | 2\.5 |  | 2\.5 |  |
SELECTORS:0:NJetFinder:MassMax| 0 |  |  | 5\.0 |  | 5 |  |
SELECTORS:0:NJetFinder:N| None |  |  | 1 |  | 1 |  |
SELECTORS:0:NJetFinder:PTMin| None |  |  | 1\.0 |  | 1 |  |
SELECTORS:0:NJetFinder:R| 0\.4 |  |  | 1\.0 |  | 1 |  |
SELECTORS:0:NJetFinder:YMax| None |  |  |  |  | 1\.79769313486e\+308 |  |
Settings kept at their default value
-------------------
The parameter listed here have not been customised, but they have been read by Sherpa during its run.

| parameter | default value |
|-|-|
1/ALPHAQED\(0\)| 137\.03599976 |  |
ABS\_ERROR| 0 |  |
AHADIC:ALPHA\_B| 14\.2 |  |
AHADIC:ALPHA\_D| 3\.4 |  |
AHADIC:ALPHA\_G| 0\.97 |  |
AHADIC:ALPHA\_H| \-0\.6 |  |
AHADIC:ALPHA\_L| 3\.9 |  |
AHADIC:BARYON\_FRACTION| 0\.17 |  |
AHADIC:BEAUTYBARYON\_ENHANCEMENT| 0\.8 |  |
AHADIC:BEAUTYCHARM\_ENHANCEMENT| 1 |  |
AHADIC:BEAUTYSTRANGE\_ENHANCEMENT| 1\.4 |  |
AHADIC:BETA\_B| 1\.6 |  |
AHADIC:BETA\_D| 0\.72 |  |
AHADIC:BETA\_H| 1\.8 |  |
AHADIC:BETA\_L| 0\.18 |  |
AHADIC:CHARMBARYON\_ENHANCEMENT| 8 |  |
AHADIC:CHARMSTRANGE\_ENHANCEMENT| 2 |  |
AHADIC:CLUSTER\_SPLITTING\_MODE| 2 |  |
AHADIC:DECAY\_THRESHOLD| 0\.02 |  |
AHADIC:DIRECT\_TRANSITIONS| 1 |  |
AHADIC:DI\_PION\_THRESHOLD| 0\.3 |  |
AHADIC:ETA\_MODIFIER| 2\.2 |  |
AHADIC:ETA\_PRIME\_MODIFIER| 4\.5 |  |
AHADIC:GAMMA\_B| 8\.1 |  |
AHADIC:GAMMA\_D| 0\.77 |  |
AHADIC:GAMMA\_H| 0\.024 |  |
AHADIC:GAMMA\_L| 0\.48 |  |
AHADIC:GLUON\_DECAY\_MODE| 0 |  |
AHADIC:KT\_0| 1\.21 |  |
AHADIC:KT\_ORDER| 0 |  |
AHADIC:MASS\_EXPONENT| 0 |  |
AHADIC:MIN\_MASS2| 0\.1 |  |
AHADIC:MULTI\_WEIGHT\_R0L0\_DELTA\_3/2| 0\.15 |  |
AHADIC:MULTI\_WEIGHT\_R0L0\_N\_1/2| 1 |  |
AHADIC:MULTI\_WEIGHT\_R0L0\_PSEUDOSCALARS| 1 |  |
AHADIC:MULTI\_WEIGHT\_R0L0\_TENSORS2| 1\.5 |  |
AHADIC:MULTI\_WEIGHT\_R0L0\_VECTORS| 2\.5 |  |
AHADIC:MULTI\_WEIGHT\_R0L1\_AXIALVECTORS| 0 |  |
AHADIC:MULTI\_WEIGHT\_R0L1\_SCALARS| 0 |  |
AHADIC:MULTI\_WEIGHT\_R0L2\_VECTORS| 0\.5 |  |
AHADIC:MULTI\_WEIGHT\_R1L0\_N\_1/2| 0\.1 |  |
AHADIC:MULTI\_WEIGHT\_R1\_1L0\_N\_1/2| 0 |  |
AHADIC:MULTI\_WEIGHT\_R2L0\_N\_1/2| 0 |  |
AHADIC:M\_BIND\_0| 0\.12 |  |
AHADIC:M\_BIND\_1| 0\.5 |  |
AHADIC:M\_BOTTOM| 5\.1 |  |
AHADIC:M\_CHARM| 1\.8 |  |
AHADIC:M\_DIQUARK\_OFFSET| 0\.3 |  |
AHADIC:M\_GLUE| 0 |  |
AHADIC:M\_STRANGE| 0\.4 |  |
AHADIC:M\_UP\_DOWN| 0\.3 |  |
AHADIC:Mixing\_0\+| \-0\.246091424531 |  |
AHADIC:Mixing\_1\-| 0\.635299847726 |  |
AHADIC:Mixing\_2\+| 0\.471238898038 |  |
AHADIC:Mixing\_3\-| 0\.5411 |  |
AHADIC:Mixing\_4\+| 0\.6283 |  |
AHADIC:OPEN\_THRESHOLD| 0\.1 |  |
AHADIC:PI\_PHOTON\_THRESHOLD| 0\.15 |  |
AHADIC:PROMPT\_DECAY\_EXPONENT| \-1 |  |
AHADIC:PT\_MAX| 0\.68 |  |
AHADIC:P\_QQ1\_by\_P\_QQ0| 0\.6 |  |
AHADIC:P\_QS\_by\_P\_QQ\_norm| 0\.56 |  |
AHADIC:P\_SS\_by\_P\_QQ\_norm| 0\.056 |  |
AHADIC:REMNANT\_CLUSTER\_MODE| 2 |  |
AHADIC:SINGLETBARYON\_MODIFIER| 1\.8 |  |
AHADIC:SINGLET\_MODIFIER| 2 |  |
AHADIC:STRANGE\_FRACTION| 0\.46 |  |
AHADIC:TRANSITION\_THRESHOLD| 0\.51 |  |
ALPHAS:FREEZE\_VALUE| 1 |  |
ALPHAS:PDF\_SET\_VERSION| 0 |  |
ALPHAS:USE\_PDF| 1 |  |
ALPHAS\(MZ\)| 0\.118 |  |
AMEGIC:ALLOW\_MAPPING| 1 |  |
AMEGIC:CHECK\_BORN| 0 |  |
AMEGIC:CHECK\_FINITE| 0 |  |
AMEGIC:CHECK\_LOOP\_MAP| 0 |  |
AMEGIC:CHECK\_POLES| 0 |  |
AMEGIC:CHECK\_THRESHOLD| 0 |  |
AMEGIC:CUT\_MASSIVE\_VECTOR\_PROPAGATORS| 1 |  |
AMEGIC:DEFAULT\_GAUGE| 1 |  |
AMEGIC:INTEGRATOR| 7 |  |
AMEGIC:LOOP\_ME\_INIT| 0 |  |
AMEGIC:ME\_LIBCHECK| 0 |  |
AMEGIC:NLO\_BVI\_MODE| 0 |  |
AMEGIC:NLO\_DR\_MODE| 0 |  |
AMEGIC:NLO\_EPS\_MODE| 0 |  |
AMEGIC:PARTIAL\_COMMIT| 0 |  |
AMEGIC:SORT\_LOPROCESS| 1 |  |
AMEGIC\_LIBRARY\_MODE| 1 |  |
AMISIC:E\(ref\)| 7000 |  |
AMISIC:Eta| 0\.16 |  |
AMISIC:MU\_F\_FACTOR| 1 |  |
AMISIC:MU\_R\_FACTOR| 0\.5 |  |
AMISIC:MU\_R\_SCHEME| PT |  |
AMISIC:PT\_0\(IR\)| 0\.5 |  |
AMISIC:PT\_0\(ref\)| 2\.05 |  |
AMISIC:PT\_Min\(ref\)| 2\.25 |  |
AMISIC:PomeronIntercept| 0\.0808 |  |
AMISIC:PomeronSlope| 0\.25 |  |
AMISIC:ReggeonIntercept| \-0\.4525 |  |
AMISIC:TriplePomeronCoupling| 0\.318 |  |
AMISIC:nMC\_points| 1000 |  |
AMISIC:nPT\_bins| 200 |  |
AMISIC:nS\_bins| 40 |  |
ANALYSIS\_OUTPUT| Analysis/ |  |
ANALYSIS\_WRITEOUT\_INTERVAL| 1\.84467440737e\+19 |  |
ASSOCIATED\_CONTRIBUTIONS\_VARIATIONS|  |  |
AS\_FORM| Smooth |  |
BATCH\_MODE| 1 |  |
BBR\_PDF\_LIBRARY|  |  |
BEAM\_1| 0 |  |
BEAM\_2| 0 |  |
BEAM\_ENERGY\_1| 0 |  |
BEAM\_ENERGY\_2| 0 |  |
BEAM\_MODE| Collider |  |
BEAM\_POLARIZATIONS| 0, 0 |  |
BEAM\_REMNANTS| 1 |  |
BEAM\_RESCATTERING| None |  |
BEAM\_SPECTRUM\_1| Monochromatic |  |
BEAM\_SPECTRUM\_2| Monochromatic |  |
BRH\_VMODE| 0 |  |
BUNCHES|  |  |
CHECK\_LIBLOCK| 0 |  |
CHECK\_SETTINGS| 1 |  |
CHECK\_WEIGHT| 0 |  |
CITATION\_DEPTH| 1 |  |
CI\_OMODE| 1 |  |
CKM:Order| 0 |  |
CKM:Output| 0 |  |
COLOUR\_RECONNECTIONS:MODE| 0 |  |
COLOUR\_SCHEME| 0 |  |
COMIX:AEXP| 0\.9 |  |
COMIX:BMODE| 1 |  |
COMIX:CHECK\_POLES| 0 |  |
COMIX:ECMODE| 2 |  |
COMIX:ITMAX| 1000000 |  |
COMIX:ITMIN| 1000 |  |
COMIX:MFAC| 1 |  |
COMIX:N\_GPL| 3 |  |
COMIX:OMODE| 3 |  |
COMIX:PARTIAL\_COMMIT| 0 |  |
COMIX:PG\_MODE| 0 |  |
COMIX:PMODE| D |  |
COMIX:PS\_ADD\_ANISO| 0 |  |
COMIX:PS\_CHTH| 0\.01 |  |
COMIX:SEXP| 1 |  |
COMIX:SPEAK| 1 |  |
COMIX:SRBASE| 1\.05 |  |
COMIX:STEXP| 0\.001 |  |
COMIX:TEXP| 0\.9 |  |
COMIX:THEXP| 1\.5 |  |
COMIX:TMODE| 1 |  |
COMIX:VINTS| 8 |  |
COMIX:VL\_MODE| 0 |  |
COMIX:VMODE| 1 |  |
COMIX:VSOPT| 1 |  |
COMIX:WF\_MODE| 0 |  |
COMIX:ZMODE| 0 |  |
COMIX\_DEFAULT\_GAUGE| 1 |  |
COMPRESS\_PARTONIC\_DECAYS| 1 |  |
COUPLINGS| Alpha\_QCD 1 |  |
DEBUG\_INTERVAL| 0 |  |
DEBUG\_STEP| \-1 |  |
DECAYER| 0 |  |
DECOMPOSE\_4G\_VERTEX| 1 |  |
DIPOLES:ALPHA| 1 |  |
DIPOLES:ALPHA\_FF| 1 |  |
DIPOLES:ALPHA\_FI| 1 |  |
DIPOLES:ALPHA\_IF| 1 |  |
DIPOLES:ALPHA\_II| 1 |  |
DIPOLES:AMIN| 1e\-08 |  |
DIPOLES:BORN\_FLAVOUR\_RESTRICTIONS|  |  |
DIPOLES:COLLINEAR\_VFF\_SPLITTINGS| 1 |  |
DIPOLES:IS\_CLUSTER\_TO\_LEPTONS| 0 |  |
DIPOLES:KAPPA| 0\.666666666667 |  |
DIPOLES:KT2MAX| 39204 |  |
DIPOLES:LIST| 0 |  |
DIPOLES:PFF\_FS\_RECOIL\_SCHEME| 0 |  |
DIPOLES:PFF\_FS\_SPLIT\_SCHEME| 0 |  |
DIPOLES:PFF\_IS\_RECOIL\_SCHEME| 0 |  |
DIPOLES:PFF\_IS\_SPLIT\_SCHEME| 1 |  |
DIPOLES:SCHEME| CSS |  |
DIPOLES:V\_SUBTRACTION\_MODE| 1 |  |
DM\_ENERGY\_DISTRIBUTION| 1 |  |
DM\_RELATIVISTIC| 1 |  |
DM\_TEMPERATURE| 1 |  |
DM\_beam\_weighted| 1 |  |
EEG:FF\_Y\_EXPONENT| 0\.5 |  |
EEG:FF\_Z\_EXPONENT| 0\.01 |  |
EEG:FI\_X\_EXPONENT| 0\.5 |  |
EEG:FI\_Z\_EXPONENT| 0\.01 |  |
EEG:IF\_U\_EXPONENT| 0\.5 |  |
EEG:IF\_X\_EXPONENT| 0\.5 |  |
EEG:II\_V\_EXPONENT| 0\.5 |  |
EEG:II\_X\_EXPONENT| 0\.5 |  |
EEG:OMODE| 2 |  |
EEG:OSTEP| 5 |  |
EEG:Q2MIN| 1e\-06 |  |
ENHANCE\_XS| 0 |  |
EPA:AlphaQED| 0\.0072992701 |  |
EPA:Debug| 0 |  |
EPA:Form\_Factor| 0 |  |
EPA:PTMin| 0 |  |
EPA:ThetaMax| 0\.3 |  |
EPA:Use\_old\_WW| 0 |  |
ERROR| 0\.01 |  |
EVENT\_DISPLAY\_INTERVAL| 100 |  |
EVENT\_INPUT|  |  |
EVENT\_OUTPUT|  |  |
EVENT\_SEED\_FILE| ran\.stat\.1234 |  |
EVENT\_SEED\_MODE| 0 |  |
EVENT\_TYPE| StandardPerturbative |  |
EVT\_FILE\_PATH| \. |  |
EVT\_OUTPUT| 2 |  |
EVT\_OUTPUT\_START| 0 |  |
EW\_REN\_SCHEME| Gmu |  |
EW\_SCHEME| Gmu |  |
EXTERNAL\_RNG| None |  |
EXTRAXS\_CSS\_APPROX\_ME| 0 |  |
FACTORIZATION\_SCALE\_FACTOR| 1 |  |
FINISH\_OPTIMIZATION| 1 |  |
FLAG\_PARTONIC\_DECAYS| 1 |  |
FRAGMENTATION| Ahadic |  |
FREEZE\_PDF\_FOR\_LOW\_Q| 0 |  |
GENERATE\_RESULT\_DIRECTORY| 1 |  |
GF| 1\.16639e\-05 |  |
GLOBAL\_KFAC| 0 |  |
GMU\_CMS\_AQED\_CONVENTION| 0 |  |
HARD\_DECAYS:Enabled| 0 |  |
HARD\_DECAYS:Spin\_Correlations| 0 |  |
HELICITY\_SCHEME| 1 |  |
HEPMC\_EXTENDED\_WEIGHTS| 0 |  |
HEPMC\_TREE\_LIKE| 0 |  |
HEPMC\_USE\_NAMED\_WEIGHTS| 1 |  |
HISTOGRAM\_OUTPUT\_PRECISION| 6 |  |
IB\_THRESHOLD\_KILL| \-1e\+12 |  |
IB\_WHBINS| 100 |  |
INCLUDE\_PHOTON\_IN\_PHOTON\_PDF| 0 |  |
INIT\_ONLY| 0 |  |
INTEGRATION\_ERROR| 0\.01 |  |
INTEGRATOR| Default |  |
INT\_MINSIJ\_FACTOR| 0 |  |
JET\_MASS\_THRESHOLD| 10 |  |
KFACTOR| None |  |
KFACTOR\_ALLOW\_MAPPING| 1 |  |
KP:ACCEPT\_NEGATIVE\_PDF| 1 |  |
KP:CHECK\_ENERGY| 0 |  |
KP:FACTORISATION\_SCHEME| 0 |  |
KP:KCONTRIB| BSGT |  |
LHEF\_PDF\_NUMBER| \-1 |  |
LOG\_FILE|  |  |
MASSIVE\_PS|  |  |
MASSLESS\_PS|  |  |
MC@NLO:DISALLOW\_FLAVOUR|  |  |
MC@NLO:FOMODE| 0 |  |
MC@NLO:HPSMODE| \-1 |  |
MC@NLO:KFACTOR\_MODE| 14 |  |
MC@NLO:KIN\_SCHEME| 1 |  |
MC@NLO:MAXEM| 1 |  |
MC@NLO:MAXWEIGHT| 1000 |  |
MC@NLO:PP\_BVI\_MODE| 7 |  |
MC@NLO:PSMODE| 0 |  |
MC@NLO:RS\_SCALE|  |  |
MCATNLO\_MASSIVE\_SPLITTINGS| 1 |  |
MCATNLO\_SPLIT\_INTO\_MASSIVE| 0 |  |
MCNLO\_DADS| 1 |  |
MEH\_EWADDMODE| 0 |  |
MEH\_NLOADD| 1 |  |
MEH\_QCDADDMODE| 0 |  |
MEMLEAK\_WARNING\_THRESHOLD| 16777216 |  |
MENLOPS\_MAX\_KFAC| 10 |  |
MEPS:ALLOW\_SCALE\_UNORDERING| 0 |  |
MEPS:CLUSTER\_MODE| 360 |  |
MEPS:CORE\_SCALE| Default |  |
MEPS:MEPS\_COLORSET\_MODE| 0 |  |
MEPS:NLO\_COUPLING\_MODE| 2 |  |
MEPS:NLO\_NMAX\_ALLCONFIGS| \-1 |  |
MEPS:NMAX\_ALLCONFIGS| \-1 |  |
MEPS:UNORDERED\_SCALE| None |  |
MEPSNLO\_PDFCT| 1 |  |
METS:CLUSTER\_MODE| 0 |  |
METS\_BBAR\_MODE| EnabledExclCluster |  |
ME\_QED:CLUSTERING\_ENABLED| 1 |  |
ME\_QED:CLUSTERING\_THRESHOLD| 10 |  |
ME\_QED:ENABLED| 1 |  |
ME\_QED:INCLUDE\_RESONANCES| 0 |  |
MI\_HANDLER| Amisic |  |
MODEL| SM |  |
MPI\_EVENT\_MODE| 0 |  |
MPI\_OUTPUT| 0 |  |
MPI\_PDF\_SET\_VERSIONS|  |  |
MPI\_PT\_MAX| 1e\+12 |  |
MPI\_PT\_Max\_Fac| 1 |  |
MPI\_SEED\_MODE| 0 |  |
MSG\_LIMIT| 20 |  |
NLOMC\_GENERATOR| CSS |  |
NLO\_IMODE| IKP |  |
NLO\_MUR\_COEFFICIENT\_FROM\_VIRTUAL| 1 |  |
NLO\_NF\_CONVERSION\_TERMS| None |  |
NLO\_SMEAR\_POWER| 0\.5 |  |
NLO\_SMEAR\_THRESHOLD| 0 |  |
NO\_ZERO\_PDF| 0 |  |
NUM\_ACCURACY| 1e\-10 |  |
N\_COLOR| 3 |  |
OL\_EXIT\_ON\_ERROR| 1 |  |
OL\_IGNORE\_MODEL| 0 |  |
OL\_PREFIX| /home/narendran/Documents/CEDAR\_stuff2/OpenLoops/\. |  |
OL\_VERBOSITY| 0 |  |
OL\_VMODE| 0 |  |
ORDER\_ALPHAS| 2 |  |
OUTPUT| 2 |  |
OUTPUT\_ME\_ONLY\_VARIATIONS| 1 |  |
OVERWEIGHT\_THRESHOLD| 1e\+12 |  |
PARTICLE\_CONTAINERS:996:ICharge| 0 |  |
PARTICLE\_CONTAINERS:996:Majorana| 0 |  |
PARTICLE\_CONTAINERS:996:Mass| 0 |  |
PARTICLE\_CONTAINERS:996:Priority| 0 |  |
PARTICLE\_CONTAINERS:996:Radius| 0 |  |
PARTICLE\_CONTAINERS:996:Spin| 0 |  |
PARTICLE\_CONTAINERS:996:Strong| 0 |  |
PARTICLE\_CONTAINERS:996:Width| 0 |  |
PARTICLE\_CONTAINERS:997:ICharge| 0 |  |
PARTICLE\_CONTAINERS:997:Majorana| 0 |  |
PARTICLE\_CONTAINERS:997:Mass| 0 |  |
PARTICLE\_CONTAINERS:997:Priority| 0 |  |
PARTICLE\_CONTAINERS:997:Radius| 0 |  |
PARTICLE\_CONTAINERS:997:Spin| 0 |  |
PARTICLE\_CONTAINERS:997:Strong| 0 |  |
PARTICLE\_CONTAINERS:997:Width| 0 |  |
PB\_USE\_FMM| 0 |  |
PDF\_SET\_VERSIONS|  |  |
POLARIZATIONS:BEAM\_1| 0 |  |
POLARIZATIONS:BEAM\_2| 0 |  |
PRETTY\_PRINT| On |  |
PRINT\_PS\_POINTS| 0 |  |
PRINT\_VERSION\_INFO| 0 |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):CKKW|  |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):Cut\_Core| 0 |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):Decay|  |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):DecayOS|  |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):No\_Decay|  |  |
PROCESSES:0:93 93 \-\> \(93,996,997\) \(93,996,997\):Sort\_Flavors| 3 |  |
PS\_PT\_FILE|  |  |
Q2\_AS| 1 |  |
RANDOM\_SEED| \-1, \-1, \-1, \-1 |  |
RANDOM\_SEED1| \-1 |  |
RANDOM\_SEED2| \-1 |  |
RANDOM\_SEED3| \-1 |  |
RANDOM\_SEED4| \-1 |  |
RELIC\_DENSITY\_EMAX| 1000000 |  |
REMNANTS:BEAM\_DECORRELATOR| 0 |  |
RENORMALIZATION\_SCALE\_FACTOR| 1 |  |
RESPECT\_MASSIVE\_FLAG| 0 |  |
RESULT\_DIRECTORY| Results |  |
RESUMMATION\_SCALE\_FACTOR| 1 |  |
REWEIGHT\_SPLITTING\_ALPHAS\_SCALES| 1 |  |
REWEIGHT\_SPLITTING\_PDF\_SCALES| 1 |  |
RIVET:\-l| 1000000 |  |
RIVET:HISTO\_INTERVAL| 0 |  |
RIVET:IGNORE\_BEAMS| 0 |  |
RIVET:JETCONTS| 0 |  |
RIVET:MATCH\_WEIGHTS|  |  |
RIVET:NLO\_SMEARING| 0 |  |
RIVET:NOMINAL\_WEIGHT|  |  |
RIVET:SKIP\_MERGE| 0 |  |
RIVET:SKIP\_WEIGHTS| 0 |  |
RIVET:SPLITCOREPROCS| 0 |  |
RIVET:SPLITPM| 0 |  |
RIVET:SPLITSH| 0 |  |
RIVET:UNMATCH\_WEIGHTS|  |  |
RIVET:USE\_HEPMC\_EXTENDED\_WEIGHTS| 0 |  |
RIVET:USE\_HEPMC\_NAMED\_WEIGHTS| 1 |  |
RIVET:USE\_HEPMC\_TREE\_LIKE| 0 |  |
RIVET:WEIGHT\_CAP| 0 |  |
RLIMIT\_AS| 539832561664 |  |
RLIMIT\_BY\_CPU| 0 |  |
RUNDATA| Sherpa\.yaml |  |
RUN\_MASS\_BELOW\_POLE| 0 |  |
SAVE\_STATUS|  |  |
SCALE\_FACTOR| 1 |  |
SCHANNEL\_ALPHA| 0\.5 |  |
SELECTION\_WEIGHT\_MODE| 0 |  |
SELECTORS:0:NJetFinder:Exp| 1 |  |
SELECTORS:0:NJetFinder:Mode| 2 |  |
SHERPA\_CPP\_PATH|  |  |
SHERPA\_LDADD|  |  |
SHERPA\_LIB\_PATH|  |  |
SHERPA\_VERSION|  |  |
SHOWER:CKFMODE| 1 |  |
SHOWER:ENHANCE|  |  |
SHOWER:EVOLUTION\_SCHEME| 3030 |  |
SHOWER:EW\_MODE| 0 |  |
SHOWER:FORCED\_DECAYS| 1 |  |
SHOWER:FORCED\_GLUON\_SCALING| \-1\.5 |  |
SHOWER:FS\_AS\_FAC| 1 |  |
SHOWER:FS\_PT2MIN| 1 |  |
SHOWER:IS\_AS\_FAC| 0\.25 |  |
SHOWER:IS\_PT2MIN| 2 |  |
SHOWER:KFACTOR\_SCHEME| 1 |  |
SHOWER:KIN\_SCHEME| 1 |  |
SHOWER:KMODE| 2 |  |
SHOWER:MASS\_THRESHOLD| 0 |  |
SHOWER:MAXEM| 18446744073709551615 |  |
SHOWER:MAXPART| 2147483647 |  |
SHOWER:MAX\_REWEIGHT\_FACTOR| 1000 |  |
SHOWER:MI\_FS\_AS\_FAC| 0\.66 |  |
SHOWER:MI\_FS\_PT2MIN| 1 |  |
SHOWER:MI\_IS\_AS\_FAC| 0\.66 |  |
SHOWER:MI\_IS\_PT2MIN| 4 |  |
SHOWER:MI\_KFACTOR\_SCHEME| 0 |  |
SHOWER:MI\_KIN\_SCHEME| 1 |  |
SHOWER:MI\_PT2MIN\_GSPLIT\_FACTOR| 1 |  |
SHOWER:OEF| 3 |  |
SHOWER:PDFCHECK| 1 |  |
SHOWER:PDF\_FAC| 1 |  |
SHOWER:PDF\_MIN| 0\.0001 |  |
SHOWER:PDF\_MIN\_X| 0\.01 |  |
SHOWER:PT2MIN\_GSPLIT\_FACTOR| 1 |  |
SHOWER:QCD\_MODE| 1 |  |
SHOWER:RECO\_CHECK| 0 |  |
SHOWER:RECO\_DECAYS| 0 |  |
SHOWER:RESPECT\_Q2| 0 |  |
SHOWER:REWEIGHT| 1 |  |
SHOWER:REWEIGHT\_SCALE\_CUTOFF| 5 |  |
SHOWER:SCALE\_FACTOR| 1 |  |
SHOWER:SCALE\_SCHEME| 14 |  |
SHOWER:SCALE\_VARIATION\_SCHEME| 1 |  |
SHOWER\_GENERATOR| CSS |  |
SHOW\_ANALYSIS\_SYNTAX| 0 |  |
SHOW\_FILTER\_SYNTAX| 0 |  |
SHOW\_KFACTOR\_SYNTAX| 0 |  |
SHOW\_ME\_GENERATORS| 0 |  |
SHOW\_MODEL\_SYNTAX| 0 |  |
SHOW\_NLOMC\_GENERATORS| 0 |  |
SHOW\_NTRIALS| 0 |  |
SHOW\_PDF\_SETS| 0 |  |
SHOW\_PS\_GENERATORS| 0 |  |
SHOW\_SCALE\_SYNTAX| 0 |  |
SHOW\_SELECTOR\_SYNTAX| 0 |  |
SHOW\_SHOWER\_GENERATORS| 0 |  |
SHOW\_VARIABLE\_SYNTAX| 0 |  |
SOFT\_COLLISIONS| None |  |
SP:ADD\_DOC| 0 |  |
SP:SET\_COLORS| 0 |  |
SPECIAL\_TAU\_SPIN\_CORRELATIONS| 0 |  |
STATUS\_PATH|  |  |
TCHANNEL\_ALPHA| 0\.9 |  |
THRESHOLD\_ALPHAS| 1 |  |
THRESHOLD\_EXPONENT| 0\.5 |  |
TIMEOUT| \-1 |  |
USERHOOKS| None |  |
USR\_WGT\_MODE| 1 |  |
VARIATIONS\_INCLUDE\_CV| 0 |  |
VEGAS\_MODE| 2 |  |
VIRTUAL\_EVALUATION\_FRACTION| 1 |  |
WIDTH\_SCHEME| CMS |  |
WRITE\_REFERENCES\_FILE| 1 |  |
YFS:1/ALPHAQED| 0 |  |
YFS:CHECK\_FIRST| 0 |  |
YFS:DRCUT| 1\.79769313486e\+308 |  |
YFS:FF\_RECOIL\_SCHEME| 2 |  |
YFS:FI\_RECOIL\_SCHEME| 2 |  |
YFS:INCREASE\_MAXIMUM\_WEIGHT| 1 |  |
YFS:IR\_CUTOFF| 0\.001 |  |
YFS:IR\_CUTOFF\_FRAME| Multipole\_CMS |  |
YFS:MAXEM| 2147483647 |  |
YFS:MINEM| 0 |  |
YFS:MODE| Full |  |
YFS:PHOTON\_SPLITTER\_ENHANCE\_FACTOR| 1 |  |
YFS:PHOTON\_SPLITTER\_MAX\_HADMASS| 0\.5 |  |
YFS:PHOTON\_SPLITTER\_MODE| 15 |  |
YFS:PHOTON\_SPLITTER\_ORDERING\_SCHEME| 2 |  |
YFS:PHOTON\_SPLITTER\_SPECTATOR\_SCHEME| 0 |  |
YFS:PHOTON\_SPLITTER\_STARTING\_SCALE\_SCHEME| 1 |  |
YFS:REDUCE\_MAXIMUM\_ENERGY| 1 |  |
YFS:STRICTNESS| 0 |  |
YFS:USE\_ME| 1 |  |
YFS:USE\_RUNNING\_PARAMETERS| 0 |  |
YFS:UV\_CUTOFF| 1\.79769313486e\+308 |  |
YUKAWA\_MASSES| Running |  |
