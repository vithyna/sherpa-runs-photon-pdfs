#include "V.H"

using namespace AMEGIC;
using namespace ATOOLS;
using namespace std;

extern "C" Values* Getter_V2_2__G__G__c__cb__O2_0__E0(Basic_Sfuncs* bs) {
  return new V2_2__G__G__c__cb__O2_0__E0(bs);
}

V2_2__G__G__c__cb__O2_0__E0::V2_2__G__G__c__cb__O2_0__E0(Basic_Sfuncs* _BS) :
     Basic_Func(0,_BS),
     Basic_Xfunc(0,_BS),
     Basic_Vfunc(0,_BS),
     Basic_Pfunc(0,_BS)
{
  f = new int[2];
  c = new Complex[4];
  Z = new Complex[93];
  M = new Complex*[16];
  for(int i=0;i<16;i++) M[i] = new Complex[3];
  cl = new int[16];
}

V2_2__G__G__c__cb__O2_0__E0::~V2_2__G__G__c__cb__O2_0__E0()
{
  if (Z)  delete[] Z;
  if (f)  delete[] f;
  if (c)  delete[] c;
  if (cl) delete[] cl;
  if (M) {
    for(int i=0;i<16;i++) delete[] M[i];
    delete[] M;
  }
}

Complex V2_2__G__G__c__cb__O2_0__E0::Evaluate(int m,int n)
{
  if (cl[n]) return M[n][m];
  switch (n) {
    case 0: Calculate_M0(); break;
    case 1: Calculate_M1(); break;
    case 2: Calculate_M2(); break;
    case 3: Calculate_M3(); break;
    case 4: Calculate_M4(); break;
    case 5: Calculate_M5(); break;
    case 6: Calculate_M6(); break;
    case 7: Calculate_M7(); break;
    case 8: Calculate_M8(); break;
    case 9: Calculate_M9(); break;
    case 10: Calculate_M10(); break;
    case 11: Calculate_M11(); break;
    case 12: Calculate_M12(); break;
    case 13: Calculate_M13(); break;
    case 14: Calculate_M14(); break;
    case 15: Calculate_M15(); break;
  }
  cl[n]=1;
  return M[n][m];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M0()
{
  M[0][0] = Z[11]*Z[12];
  M[0][1] = (Z[7]*Z[17]-Z[16]*Z[15])*Z[19];
  M[0][2] = -(Z[7]*Z[20]-Z[23]*Z[22])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M1()
{
  M[1][0] = Z[31]*Z[12];
  M[1][1] = (Z[29]*Z[17]-Z[33]*Z[15])*Z[19];
  M[1][2] = -(Z[29]*Z[20]-Z[36]*Z[22])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M2()
{
  M[2][0] = Z[42]*Z[12];
  M[2][1] = (Z[7]*Z[45]-Z[14]*Z[43])*Z[19];
  M[2][2] = -(Z[7]*Z[46]-Z[25]*Z[48])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M3()
{
  M[3][0] = Z[50]*Z[12];
  M[3][1] = (Z[29]*Z[45]-Z[32]*Z[43])*Z[19];
  M[3][2] = -(Z[29]*Z[46]-Z[37]*Z[48])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M4()
{
  M[4][0] = Z[54]*Z[12];
  M[4][1] = (Z[18]*Z[17]-Z[16]*Z[56])*Z[19];
  M[4][2] = -(Z[18]*Z[20]-Z[58]*Z[22])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M5()
{
  M[5][0] = Z[60]*Z[12];
  M[5][1] = (Z[34]*Z[17]-Z[33]*Z[56])*Z[19];
  M[5][2] = -(Z[34]*Z[20]-Z[62]*Z[22])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M6()
{
  M[6][0] = Z[65]*Z[12];
  M[6][1] = (Z[18]*Z[45]-Z[14]*Z[66])*Z[19];
  M[6][2] = -(Z[18]*Z[46]-Z[59]*Z[48])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M7()
{
  M[7][0] = Z[68]*Z[12];
  M[7][1] = (Z[34]*Z[45]-Z[32]*Z[66])*Z[19];
  M[7][2] = -(Z[34]*Z[46]-Z[63]*Z[48])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M8()
{
  M[8][0] = Z[72]*Z[12];
  M[8][1] = (Z[21]*Z[17]-Z[74]*Z[15])*Z[19];
  M[8][2] = -(Z[21]*Z[20]-Z[23]*Z[75])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M9()
{
  M[9][0] = Z[77]*Z[12];
  M[9][1] = (Z[35]*Z[17]-Z[79]*Z[15])*Z[19];
  M[9][2] = -(Z[35]*Z[20]-Z[36]*Z[75])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M10()
{
  M[10][0] = Z[81]*Z[12];
  M[10][1] = (Z[21]*Z[45]-Z[73]*Z[43])*Z[19];
  M[10][2] = -(Z[21]*Z[46]-Z[25]*Z[83])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M11()
{
  M[11][0] = Z[84]*Z[12];
  M[11][1] = (Z[35]*Z[45]-Z[78]*Z[43])*Z[19];
  M[11][2] = -(Z[35]*Z[46]-Z[37]*Z[83])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M12()
{
  M[12][0] = Z[88]*Z[12];
  M[12][1] = (Z[57]*Z[17]-Z[74]*Z[56])*Z[19];
  M[12][2] = -(Z[57]*Z[20]-Z[58]*Z[75])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M13()
{
  M[13][0] = Z[89]*Z[12];
  M[13][1] = (Z[61]*Z[17]-Z[79]*Z[56])*Z[19];
  M[13][2] = -(Z[61]*Z[20]-Z[62]*Z[75])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M14()
{
  M[14][0] = Z[91]*Z[12];
  M[14][1] = (Z[57]*Z[45]-Z[73]*Z[66])*Z[19];
  M[14][2] = -(Z[57]*Z[46]-Z[59]*Z[83])*Z[26];
}

void V2_2__G__G__c__cb__O2_0__E0::Calculate_M15()
{
  M[15][0] = Z[92]*Z[12];
  M[15][1] = (Z[61]*Z[45]-Z[78]*Z[66])*Z[19];
  M[15][2] = -(Z[61]*Z[46]-Z[63]*Z[83])*Z[26];
}

