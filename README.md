# Sherpa Runs Photon PDFs

Collection of Sherpa tests with LHAPDF and Rivet analyses found in my [LHAPDF](https://gitlab.com/vithyna/lhapdf_graphing) and [RIVET](https://gitlab.com/vithyna/my-rivet-analyses) repos.

## Content

### Zeus Analysis tests
- test 1 - broken run with sherpa 2
- test 2 - broken run with sherpa 2
- test 3 - working run with sherpa3 alpha. Currently, the ZEUS prototype analysis doesn't produce a result (all zeroes). But the validated OPAL one does. This means sherpa works (phew) but either the analysis I wrote is broken, or 11,-11 beams don't work (reasonble?). Trying with e-p beams in test 4
- test 4 - sherpa 3 run for an ep beam. Same Rivet analyses as before.
- test 5 - sherpa 3 run duplicate of test3 but with higher energy beams to test issue raised in test 3.
- test 6 - same as 4, but with much better runcard, that should actually work
- test 7 - These are like 6, but for direct process (with no PDF specified for Photon) and also contains combined (via `rivet-merge`) direct + resolved, where the resolved process comes from test_6/results9,10,11.
- test 8 - Copy of 6 for testing the new histograms added to the ZEUS_1997 analysis. Merged_final_run is the rivet-merge of the "final" rivet ZEUS_1997 analysis with both direct + resolved. Direct is 450k runs and resolved is 470k runs.
- results3 - EtMin: 4.0 and 450k events
- results4 - same as 3 but with SAS-G from lha5to6 conversion
- results5 - same as 4 but with SAS-G member 1 rather than 0
- results6 - same as 4 but with SAS-G member 2 rather than 0
- results7 - same as 4 but with SAS-G member 3 rather than 0
- results8 - something to be forgotten
- results9 - EtMin:4.0 450k events with SaS-G lha5to6 converted member 1
- resutls10 - same as 9 but with member 0
- results11 - same as 9 but with Sherpa's version of SaS-G
- results12 - EtMin:4.0 450k events with SaS-G lha5to6 converted member 0 with the P2=100 slice
- results13 - EtMin:4.0 450k events with SaS-G lha5to6 converted member 0 with the P2=10000 slice
#### Test7 results
The Analysis.yoda yodafile is for the direct process with 450k runs and EtMin:4.0. The other files (ending with _plusdirect) are the `rivet-merge` merged yodafiles with different PDFs for the resolved process. The graphs and .dat files are in lha0, lha1 and sherpasas directories. The only graph graph and datapoints of value in these are the `x_obs_gamma` ones.

### H1 Analysis tests
Odd numbers are resolved process and the even numbers are direct equivalant, starting with test1,2
- test 1 - preliminary tests to set up experiment and test a few of the first analyses.
- test3,4 - run for first validation of fully completed analysis
- test5,6 - tests with OUR (hooray) pdfs, and testing the PDF_VARIATIONS.

## Installation

### Sherpa 2.x

```
./configure --with-sqlite3=install --enable-pythia --enable-lhapdf=/home/anarendran/Documents/CEDAR_stuff/LHAPDF-6.5.3/install --enable-fastjet=/home/anarendran/Documents/temp/local --enable-hepmc3=/home/anarendran/Documents/temp/local --enable-rivet=/home/anarendran/Documents/temp/local --enable-mpi
make install
```

### Sherpa 3.0.0 alpha1
```
autoreconf -fi
./configure --enable-pythia --with-lhapdf=/home/anarendran/Documents/CEDAR_stuff/LHAPDF-6.5.3/install --enable-hepmc3=/home/anarendran/Documents/temp/local --enable-rivet=/home/anarendran/Documents/temp/local --enable-mpi --with-libzip=install --enable-multithread
make && make install

```

### Sherpa 3.0.0 alpha1 with Cmake
```
cmake -S . -B build    -DCMAKE_INSTALL_PREFIX=$PWD/local   -DCMAKE_CXX_STANDARD=17   -DSHERPA_ENABLE_INTERNAL_PDFS=ON   -DSHERPA_ENABLE_RIVET=ON   -DSHERPA_ENABLE_HEPMC3=ON -DSHERPA_ENABLE_INSTALL_LIBZIP=ON -DRIVET_ROOT_DIR=/home/anarendran/Documents/temp/local
cmake --build build/ -j16
cmake --install build/
```

## Notes and comments
### test6
I can technically set etamax to 2, as I am cutting out all jets below 1.875 away anyway? (need to verify)
